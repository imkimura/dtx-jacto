# DTX - RH Jacto

![](https://media.licdn.com/dms/image/C510BAQF4WaAeV2kUPw/company-logo_200_200/0?e=2159024400&v=beta&t=sz7DYhrt08ZaA5CZaUs8KqMTT6tGV2bmPGVIO_3-Qdc)


## Index


<!--ts-->
   * [Requisitos](#requisitos)
   * [Começando Projeto](#começando-projeto)
   * [Links](#links)
   * [Sprints](#sprints)

<!--te-->


----

## Requisitos

- Python >= 3...
- pip
- virtualenv
- django

----

## Começando Projeto

- Instalar python em seu computador. (https://www.python.org/)

- Após instalar o python, o configure nas variáveis de ambiente (PATH)

![](https://python.org.br/images/instalacao-windows/add-python-to-path.png)

- Após isso baixe o pip -> https://bootstrap.pypa.io/get-pip.py (gerenciador de pacotes que utilizaremos do python). <br>
- Vá ate o diretório onde o arquivo **get-pip.py** foi instalado e execute o comando:

    `$ python get-pip.py`

- Clone o projeto dando o comando

```git
git clone <link repositorio>
```
```git
git clone https://gitlab.com/BDAg/dtx---jacto-rh.git
```

- Va ate a pasta "dtx---jacto-rh" e instale o virtualenv nela

`$ pip install virtualenv `
<br>
`$ python -m venv venv`

- Após isso é necessário ativar o virtualenv

```cmd
cd dtx---jacto-rh/venv/Scripts/activate.bat
```
Sua linha de comando ficará mais ou menos assim...
<br>
<br>
**(venv) C:\Users\\`<Usuário>`\\`<Caminho ate o projeto>`\ dtx---jacto-rh\venv\Scripts**
<br>


- Volta para a pasta do projeto e **com o venv ativado** instale as bibliotecas no arquivo requirements.txt

`$ pip install -r requirements.txt`

- Finalmente para rodar o projeto execute o comando

`$ python manage.py runserver`

----

## Links

`<projetoDTX>` : <https://gitlab.com/BDAg/dtx---jacto-rh>

----

## Sprints

- [x] MVP
- [ ] FrontEnd Telas
- [ ] CRUD Telas


